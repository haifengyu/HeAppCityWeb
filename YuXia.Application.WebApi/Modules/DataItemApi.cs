﻿using YuXia.Application.Base.SystemModule;
using Nancy;

namespace YuXia.Application.WebApi.Modules
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2018.01.04
    /// 描 述：数据字典
    /// </summary>
    public class DataItemApi: BaseApi
    {
        /// <summary>
        /// 注册接口
        /// </summary>
        public DataItemApi()
            : base("/YuXia/adms/dataitem")
        {
            Get["/details"] = GetDetailList;// 获取数据字典详细列表
        }
        private DataItemIBLL dataItemIBLL = new DataItemBLL();

        /// <summary>
        /// 获取数据字典详细列表
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response GetDetailList(dynamic _)
        {
            string req = this.GetReqData();// 获取模板请求数据
            var data = dataItemIBLL.GetDetailList(req, "");
            return Success(data);
        }

    }
}