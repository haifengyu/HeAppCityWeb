﻿using YuXia.Application.Base.OrganizationModule;
using Nancy;
using System.Collections.Generic;

namespace YuXia.Application.WebApi.Modules
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2018.01.04
    /// 描 述：组织机构数据接口
    /// </summary>
    public class OrganizationApi : BaseApi
    {
        /// <summary>
        /// 注册接口
        /// </summary>
        public OrganizationApi()
            : base("/YuXia/adms/organization")
        {
            Get["/map"] = GetMapList;// 获取组织机构数据映射表
        }
        private DepartmentIBLL departmentIBLL = new DepartmentBLL();
        private CompanyIBLL companyIBLL = new CompanyBLL();
        private UserIBLL userIBLL = new UserBLL();


        /// <summary>
        /// 获取组织机构数据映射表
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response GetMapList(dynamic _)
        {
            List<CompanyEntity> companylist = companyIBLL.GetList();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            foreach (var company in companylist)
            {
                var item = new
                {
                    name = company.F_FullName,
                    parentId = company.F_ParentId,
                    type = 0 // 0表示公司 1 表示部门 2表示人员
                };
                dic.Add(company.F_CompanyId, item);
                // 获取当前公司下的人员
                List<DepartmentEntity> departmentList = departmentIBLL.GetList(company.F_CompanyId);
                foreach (var department in departmentList)
                {
                    var item2 = new
                    {
                        name = department.F_FullName,
                        parentId = department.F_ParentId,
                        companyId = company.F_CompanyId,
                        type = 1 // 0表示公司 1 表示部门 2表示人员
                    };
                    dic.Add(department.F_DepartmentId, item2);
                }
                // 获取当前公司下的人员
                List<UserEntity> userList = userIBLL.GetList(company.F_CompanyId);
                foreach (var user in userList)
                {
                    var item3 = new
                    {
                        name = user.F_RealName,
                        departmentId = user.F_DepartmentId,
                        companyId = company.F_CompanyId,
                        type = 1 // 0表示公司 1 表示部门 2表示人员
                    };
                    dic.Add(user.F_UserId, item3);
                }
            }
            return Success(dic);
        }
    }
}