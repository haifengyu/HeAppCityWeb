﻿// 力软信息
// 力软框架开发组
var YuXia = {};
(function ($, YuXia) {
    "use strict";

    // 配置
    YuXia.config = {
        debug: true, // 是否是调试模式（此模式下页面数据从）


    };


    // 模块数据缓存
    YuXia.modules = {};

    YuXia.app = {
        init: function () {
            // 初始化返回键点回事件
            $('#lr_containers').delegate(".lr-header-back", "click", function (e) {
                var path = $(this).parents('.lr-page')[0]._path;
                console.log(path);
                YuXia.app.backNav(path);
            });
            YuXia.tab.init();
            YuXia.tab.setTab('demo');
        },
        nav: function (op) {
            var dfop = {
                path: '',
                title: '',
                isBack: true,
                isHead: true,
                isTab: false,
                transition: ''
            }
            $.extend(dfop, op || {});

            if (dfop.path == '') {
                return false;
            }

            var $oldPage = $('.lr-page.active');
            var oldPath = '';
            if ($oldPage.length > 0) {
                oldPath = $oldPage[0]._path;
                if (oldPath == dfop.path) {
                    return false;
                }
            }
            var module;

            if (!!YuXia.modules[dfop.path]) {
                module = YuXia.modules[dfop.path];
            }
            else {
                var id = YuXia.newGuid().replace(/-/g, '_');

                module = {
                    id: id,
                    $page: $('<div class="lr-page ' + "lr-page-" + dfop.path.replace('/','_') +'" id="page_' + id + '"  ></div>'),
                    $content: $('<div class="lr-page-content"  ></div>'),
                    htmlLoaded: false,
                    cssLoaded: false,
                    jsLoaded: false
                };
                YuXia.modules[dfop.path] = module;
                YuXia.app.loadpage(dfop.path);

                if (dfop.title != '') { // 添加页面头部
                    module.$page.addClass('lr-page-header');
                    var $header = $('<div class="lr-header"></div>');
                    if (dfop.isBack) {
                        $header.append('<div class="lr-header-back" ><i class="iconfont icon-back_light"></i></div>');
                    }
                    var $title = $('<div class="lr-header-title">' + dfop.title + '</div>');
                    $header.append($title);
                    module.$page.append($header);
                }
                // 添加页面标题
                module.$page.append(module.$content);
                $('#lr_containers').append(module.$page);
            }
            // 添加之前的页面名称用于返回
            module.$page[0]._path = dfop.path;
            module.backPage = oldPath;
            module.transition = dfop.transition;
            module.isTab = dfop.isTab;

            var $page = module.$page;

            // 设置过度动画
            var param;
            switch (dfop.transition) {
                case 'right':
                    $page.css({ 'top': '0px', 'left': '100%' });
                    param = { 'left': '0px' };
                    break;
                case 'bottom':
                    $page.css({ 'top': '100%', 'left': '0px' });
                    param = { 'top': '0px' };
                    break;
                default:
                    $page.css({ 'top': '0px', 'left': '0px' });
                    break;
            }
            if (!!param) {
                $page.css('z-index', '11');
                $page.css('display', 'block');
                $oldPage.animate({ 'display': 'none' }, 200, function () {
                    $oldPage.removeClass('active');
                });
                $page.animate(param, 200, function () {

                    $page.addClass('active');
                    if (dfop.isTab) {
                        $page.addClass('lr-page-tab');
                        $('.lr-tabbar').show();
                    }
                    else {
                        $page.removeClass('lr-page-tab');
                        $('.lr-tabbar').hide();
                    }
                    $page.css('z-index', '1');
                });
            }
            else {
                $oldPage.css('display', 'none');
                $page.css('display', 'block');
                $page.addClass('active');
                $oldPage.removeClass('active');
                $page.css('z-index', '1');
                if (dfop.isTab) {
                    $page.addClass('lr-page-tab');
                    $('.lr-tabbar').show();
                }
                else {
                    $page.removeClass('lr-page-tab');
                    $('.lr-tabbar').hide();
                }
            }
        },
        backNav: function (currentPath) {
            var module = YuXia.modules[currentPath] || {};
          
            if (!!module.backPage) {
                var oldModule = YuXia.modules[module.backPage] || {};
                var $page = module.$page;
                var $oldPage = oldModule.$page;

                var param;
                switch (module.transition) {
                    case 'right':
                        param = { 'left': '100%' };
                        break;
                    case 'bottom':
                        param = { 'top': '100%' };
                        break;
                    default:
                        break;
                }

                if (!!param) {
                    $page.css('z-index', '11');
                    $page.css('display', 'block');
                    $oldPage.animate({ 'display': 'block' }, 200, function () {
                        $oldPage.addClass('active');
                    });
                    $page.animate(param, 200, function () {

                        $page.css('z-index', '1');
                        $page.removeClass('active');
                        $page.css('display', 'none');
                    });
                }
                else {
                    $page.css('display', 'none');
                    $oldPage.css('display', 'block');
                    $page.removeClass('active');
                    $oldPage.addClass('active');
                }

                if (oldModule.isTab) {
                    $('.lr-tabbar').show();
                }
                else {
                    $('.lr-tabbar').hide();
                }
            }
        },
        loadpage: function (path) {// 加载页面
            if (YuXia.config.debug) {
                // 加载页面css代码
                YuXia.ajax.httpAsync('GET', 'pages/' + path + '/index.css', {}, function (res) {
                    $('body').append('<style>' + res + '</style>');
                    YuXia.modules[path].cssLoaded = true;
                }, 'text');
                // 加载页面html代码
                YuXia.ajax.httpAsync('GET', 'pages/' + path + '/index.html', {}, function (res) {
                    YuXia.modules[path].$content.html(res);
                    YuXia.modules[path].htmlLoaded = true;
                }, 'text');
                // 加载页面js代码
                YuXia.ajax.httpAsync('GET', 'pages/' + path + '/index.js', {}, function (res) {
                    function _isload(_path, _js) {
                        if (YuXia.modules[_path].htmlLoaded) {
                            $('body').append('<script>(function ($page) { "use strict";' + _js + '})($("#page_' + name + '"));</script>');
                            YuXia.modules[_path].jsLoaded = true;
                        }
                        else {
                            setTimeout(function () {
                                _isload(_path, _js);
                            }, 100);
                        }
                    }
                    _isload(path, res);
                }, 'text');
            }
            else
            {

            }
        }
    };

    // ajax 通信
    YuXia.ajax = {// ajax 通信类
        httpAsync: function (type, url, param, callback, dataType) {
            $.ajax({
                url: url,
                data: param,
                type: type,
                dataType: dataType || "json",
                async: true,
                cache: false,
                success: function (res) {
                    callback(res);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    callback(null);
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        }
    };

    YuXia.tab = {
        init: function () {
            $('.lr-tabbar').delegate(".lr-tab-button", "click", function (e) {
                var $this = $(this);
                if ($this.hasClass('active')) {
                    return false;
                }
                var page = $this.attr('data-value');
                var title = $this.find('span').text();

                YuXia.app.nav({ path: page, isBack: false, title: title, isTab: true });

                $this.parent().find('.active').removeClass('active');
                $this.addClass('active');
            });
        },
        setTab: function (page) {
            $('[data-value="' + page + '"]').trigger('click');
        }
    };

    // 创建一个GUID
    YuXia.newGuid = function () {
        var guid = "";
        for (var i = 1; i <= 32; i++) {
            var n = Math.floor(Math.random() * 16.0).toString(16);
            guid += n;
            if ((i == 8) || (i == 12) || (i == 16) || (i == 20)) guid += "-";
        }
        return guid;
    };

    // 移动端插件
    // 开关
    $.fn.lrswitch = function (op) {
        $(this).each(function () {
            var $this = $(this);
            $this.on('click', function () {
                if ($this.hasClass('lr-active')) {
                    $this.removeClass('lr-active');
                }
                else {
                    $this.addClass('lr-active');
                }
            });
        });
       
    }
    // 选择框
     


})(jQuery, YuXia);




// 有关“空白”模板的简介，请参阅以下文档:
// http://go.microsoft.com/fwlink/?LinkID=397704
// 若要在 cordova-simulate 或 Android 设备/仿真器上在页面加载时调试代码: 启动应用，设置断点，
// 然后在 JavaScript 控制台中运行 "window.location.reload()"。
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // 处理 Cordova 暂停并恢复事件
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // TODO: Cordova 已加载。在此处执行任何需要 Cordova 的初始化。
        YuXia.app.init();
        //YuXia.app.nav({ name: 'login', isBack: false });
    };

    function onPause() {
        // TODO: 此应用程序已挂起。在此处保存应用程序状态。
    };

    function onResume() {
        // TODO: 此应用程序已重新激活。在此处还原应用程序状态。
    };
} )();