﻿(function () {
    var colors = ['bgcblue1', 'bgcblue2', 'bgcyellow', 'bgcorange', 'bgcpink', 'bgccyan', 'bgcpurple'];

    var page = {
        isScroll: true,
        init: function ($page) {
            // 常见任务点击事件
            $page.find('#commonapp .appitem').on('tap', function () {
                var $this = $(this);
                var value = $this.attr('data-value');
                var title = $this.find('span').text();
                YuXia.nav.go({ path: 'workflow/' + value, title: title, type: 'right' });
            });
            // 加载流程模板数据
            YuXia.httpget(config.workapi + "/workflow/schemelist", null, (res) => {
                if (res == null) {
                    return;
                }
                console.log(res);

                if (res.code == 200) {
                    if (res.data.length > 0) {
                        var $this = $('.appbox');
                        $this.append(' <div class="title">流程发起</div>');
                        var _html = '<div class="applist" id="workflowshcemelist">';
                        $.each(res.data, function (_index, _item) {
                            var colorindex = _index % 7;
                            _html += '<div class="appitem" data-value="' + _item.F_Code+'" ><div class="' + colors[colorindex] + '" > <i class="iconfont icon-news_hot_light"></i></div ><span>' + _item.F_Name + '</span></div >';
                        });
                        _html += '</div>';
                        $this.append(_html);

                        $('#workflowshcemelist .appitem').on('tap', function () {
                            var $this = $(this);
                            var value = $this.attr('data-value');
                            var title = $this.find('span').text();
                            YuXia.nav.go({ path: 'workflow/taskform', title: title, type: 'right', param: { schemeCode: value, type: 0 } });
                        });
                    }
                }
                else {
                }
            });

        }
    };
    return page;
})();