﻿(function () {
    var processId = '';
    var taskId = '';

    var fieldMap = {};
    var formMap = {};
    var page = {
        isScroll: false,
        init: function ($page, param) {
            processId = param.processId;
            taskId = param.taskId;
            taskinfo(param);

            // 提交流程
            $page.find('#auditbtn').on('tap', function () {
                var des = $('#lrflowworkdes').text();
                var verify = $('#lrflowworkverify').lrpickerGet();
                if (!!verify) {
                     // 保存表单数据
                    var formData = $('#auditcontainer').custmerformGet();
                    var formreq = [];
                    var formAllData = {};
                    for (var id in formData) {
                        if (!fieldMap[id]) {
                            YuXia.layer.warning('未设置流程表单关联字段！', function () { }, '力软提示', '关闭');
                            return false;
                        }
                        $.extend(formAllData, formData[id]);

                        if (!formMap[id]) {
                            formData[id][fieldMap[id]] = processId;
                        }
                     

                        var point = {
                            schemeInfoId: id,
                            processIdName: fieldMap[id],
                            //keyValue: processId,
                            formData: JSON.stringify(formData[id])
                        }

                        if (formMap[id]) {
                            point.keyValue = processId;
                        }

                        formreq.push(point);
                    }


                    YuXia.layer.loading(true, "正在提交数据");
                    YuXia.httppost(config.webapi + "/YuXia/adms/form/save", formreq, (res) => {
                        if (res.code == 200) {// 表单数据保存成功，发起流程

                            var flowreq = {
                                taskId: taskId,
                                verifyType: verify,
                                description: des,
                                formData: JSON.stringify(formAllData)
                            };

                            YuXia.httppost(config.workapi + "/workflow/audit", flowreq, (res) => {
                                if (res.code == 200) {
                                    YuXia.layer.loading(false);
                                }
                                else {// 接口异常

                                }
                                YuXia.nav.closeCurrent();
                            });
                        }
                        else {// 接口异常

                        }
                        YuXia.layer.loading(false);
                    });
                }
                else {
                    YuXia.layer.warning('请选择审核结果！', function () { }, '力软提示', '关闭');
                }
            });

        }
    };
    // 流程发起初始化
    function taskinfo(_param) {
        var req = {
            processId: _param.processId,
            taskId: _param.taskId
        };
        YuXia.layer.loading(true, "获取流程信息");
        YuXia.httppost(config.workapi + "/workflow/taskinfo", req, (res) => {
            //console.log(res,'流程信息');
            if (res.code == 200) {
                var flowdata = res.data;
                if (flowdata.status == 1) {// 流程数据加载成功
                    var wfForms = flowdata.data.currentNode.wfForms;// 表单数据
                    //console.log(wfForms,'当前节点关联的表单数据');
                    // 获取下关联字段
                    var formreq = [];
                    $.each(wfForms, function (_index, _item) {
                        fieldMap[_item.formId] = _item.field;
                        var point = {
                            schemeInfoId: _item.formId,
                            processIdName: _item.field,
                            keyValue: _param.processId,
                        }
                        formreq.push(point); 
                    });

                    // 获取下自定义表单数据
                    YuXia.httpget(config.webapi + "/YuXia/adms/form/data", formreq, (res) => {
                        console.log(res, '获取到的表单数据');
                        if (res.code == 200) {// 加载表单
                            // 设置自定义表单数据
                            $.each(res.data, function (_id, _item) {
                                $.each(_item, function (_j, _jitem) {
                                    if (_jitem.length > 0) {
                                        formMap[_id] = true;
                                    }
                                });
                            });
                            $('#auditcontainer').custmerformSet(res.data);

                        }
                    });

                    $('#auditcontainer').custmerform(wfForms, 1);
                }
            }
            else {// 接口异常

            }
            YuXia.layer.loading(false);
        });
    }

    return page;
})();