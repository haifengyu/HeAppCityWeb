﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-08-29 17:06
 * 描  述：订单列表查看
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, YuXia) {
    "use strict";
    var selectedRow = YuXia.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/Orders/OrdersList/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                        }
                        else {
                            $('[data-table="' + id + '"]').lrSetFormData(data[id]);
                        }
                    }
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').lrValidform()) {
            return false;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Orders"]').lrGetFormData());
        postData.strorderProductsEntity = JSON.stringify($('[data-table="OrderProducts"]').lrGetFormData());
        postData.strproductEntity = JSON.stringify($('[data-table="Product"]').lrGetFormData());
        postData.strshopEntity = JSON.stringify($('[data-table="Shop"]').lrGetFormData());
        postData.struserEntity = JSON.stringify($('[data-table="User"]').lrGetFormData());
        $.lrSaveForm(top.$.rootUrl + '/Orders/OrdersList/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
