﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-08-29 17:06
 * 描  述：订单列表查看
 */
var refreshGirdData;
var bootstrap = function ($, YuXia) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
            page.selectinit();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').lrdate({
                dfdata: [
                    { name: '今天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                YuXia.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Orders/OrdersList/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('OrderID');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Orders/OrdersList/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('OrderID');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            YuXia.deleteForm(top.$.rootUrl + '/Orders/OrdersList/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/Orders/OrdersList/GetPageList',
                headData: [
                    { label: "订单编号", name: "OrderNum", width: 160, align: "left"},
                    { label: "订单状态", name: "OrderStatusStr", width: 80, align: "left"},
                    { label: "商品价格", name: "Price", width: 80, align: "left"},
                    { label: "商品数量", name: "Count", width: 80, align: "left"},
                    { label: "快递费", name: "ExpressFee", width: 80, align: "left"},
                    { label: "订单总价", name: "Total", width: 80, align: "left"},
                    { label: "商铺名称", name: "ShopName", width: 160, align: "left"},
                    { label: "产品名称", name: "ProductName", width: 160, align: "left"},
                    { label: "订单用户", name: "UserName", width: 160, align: "left"},
                    { label: "下单时间", name: "SubmitDate", width: 160, align: "left"},
                    //{ label: "ShopID", name: "ShopID", width: 160, align: "left"},
                    //{ label: "ProductID", name: "ProductID", width: 160, align: "left"},
                    //{ label: "UserID", name: "UserID", width: 160, align: "left"},
                    //{ label: "OrderID", name: "OrderID", width: 160, align: "left"},
                    //{ label: "OrderStatus", name: "OrderStatus", width: 160, align: "left"},
                ],
                mainId:'OrderID',
                reloadSelected: true,
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        },
        selectinit: function () {
            var dfop2 = {
                // 字段
                value: "OrderStatus",
                text: "OrderStatusName",
                title: "OrderStatusName",
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Orders/OrdersList/GetOrderStatusEnumlist',
                // 访问数据接口参数
                param: { parentId: '' },
            }

            $('#OrderStatus').lrselect(dfop2);
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
