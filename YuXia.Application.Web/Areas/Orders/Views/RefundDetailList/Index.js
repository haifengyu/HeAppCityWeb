﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-09-10 12:08
 * 描  述：退货明细列表
 */
var refreshGirdData;
var bootstrap = function ($, YuXia) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
            page.selectinit();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').lrdate({
                dfdata: [
                    { name: '今天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                YuXia.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Orders/RefundDetailList/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                //var tabTemp = $('#lr_tab_99999');
                    //if (tabTemp != null && tabTemp != undefined) {
                    //    YuXia.frameTab.close(99999);
                    //}
                var keyValue = $('#girdtable').jfGridValue('RefundId');
                if (YuXia.checkrow(keyValue)) {
                    var urlAddress = 'http://192.168.31.106:8081/PayIndex/RefundIndex?refundId=' + keyValue;
                    window.open(urlAddress);
                    
                    //YuXia.frameTab.open({ F_ModuleId: '99999', F_Icon: 'fa fa-desktop', F_FullName: '支付模板', F_UrlAddress: urlAddress }, false);

                    //YuXia.layerForm({
                    //    id: 'form',
                    //    title: '退款',
                    //    url: 'http://192.168.31.106:8081/PayIndex/RefundIndex?refundId=' + keyValue,
                    //    width: 600,
                    //    height: 400,
                    //    callBack: function (id) {
                    //        return top[id].acceptClick(refreshGirdData);
                    //    }
                    //});
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('RefundId');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            YuXia.deleteForm(top.$.rootUrl + '/Orders/RefundDetailList/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/Orders/RefundDetailList/GetPageList',
                headData: [
                    { label: "订单编号", name: "OrderNumbers", width: 160, align: "left" },
                    { label: "第三方支付编号", name: "TradeNo", width: 160, align: "left" },
                    { label: "支付方式", name: "RefundMethod", width: 160, align: "left"},
                    { label: "退货金额", name: "RefundAmount", width: 160, align: "left"},
                    { label: "申请退货日期", name: "RefundDate", width: 160, align: "left"},
                    { label: "退货原因", name: "ReturnReason", width: 160, align: "left" },
                    { label: "退款状态", name: "RefundStatusStr", width: 160, align: "left" },
                    { label: "订单状态", name: "OrderStatusStr", width: 160, align: "left"},
                    { label: "支付状态", name: "PaymentStatusStr", width: 160, align: "left" },
                    { label: "买家用户", name: "UserName", width: 160, align: "left" },
                ],
                mainId:'RefundId',
                reloadSelected: true,
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        },
        selectinit: function () {
            var dfop2 = {
                // 字段
                value: "OrderStatus",
                text: "OrderStatusName",
                title: "OrderStatusName",
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Orders/OrdersList/GetOrderStatusEnumlist',
                // 访问数据接口参数
                param: { parentId: '' },
            }

            var dfop3 = {
                // 字段
                value: "PaymentStatus",
                text: "PaymentStatusName",
                title: "PaymentStatusName",
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Orders/PaymentDetailList/GetPaymentStatusEnumlist',
                // 访问数据接口参数
                param: { parentId: '' },
            }

            var dfop4 = {
                // 字段
                value: "RefundStatus",
                text: "RefundStatusName",
                title: "RefundStatusName",
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Orders/RefundDetailList/GetRefundStatusEnumlist',
                // 访问数据接口参数
                param: { parentId: '' },
            }

            $('#OrderStatus').lrselect(dfop2);
            $('#PaymentStatus').lrselect(dfop3);
            $('#RefundStatus').lrselect(dfop4);
            $('[data-table=RefundStatusValue]').lrSetFormData(1);
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
