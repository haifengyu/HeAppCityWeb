﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-08-30 14:27
 * 描  述：商品列表
 */
var acceptClick;
var keyValue = request('keyValue');
var bootstrap = function ($, YuXia) {
    "use strict";
    var selectedRow = YuXia.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.selectinit();
            page.bind();
            page.initData();
        },
        bind: function () {
        },
        initData: function () {
            if (!!keyValue) {
                $.lrSetForm(top.$.rootUrl + '/Orders/ProductsList/GetFormData?keyValue=' + keyValue, function (data) {
                    for (var id in data) {
                        if (!!data[id].length && data[id].length > 0) {
                        }
                        else {
                            $('[data-table="' + id + '"]').lrSetFormData(data[id]);
                        }
                    }
                });
            }
        },

        selectinit: function () {
            var dfop2 = {
                // 字段
                value: "ShopID",
                text: "ShopName",
                title: "ShopName",
                // 展开最大高度
                maxHeight: 200,
                // 是否允许搜索
                allowSearch: true,
                // 访问数据接口地址
                url: top.$.rootUrl + '/Orders/ProductsList/GetShopsList',
                // 访问数据接口参数
                param: { parentId: '' },
            }

            $('#ShopID').lrselect(dfop2);
        }
    };
    $('#ImageURL').lrUploaderIsHeCity({ type: "products" });
    

    // 保存数据
    acceptClick = function (callBack) {
        if (!$('body').lrValidform()) {
            return false;
        }
        var postData = {};
        postData.strEntity = JSON.stringify($('[data-table="Product"]').lrGetFormData());
        postData.strproductDetailEntity = JSON.stringify($('[data-table="ProductDetail"]').lrGetFormData());
        postData.strshopEntity = JSON.stringify($('[data-table="Shop"]').lrGetFormData());
        $.lrSaveForm(top.$.rootUrl + '/Orders/ProductsList/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}
