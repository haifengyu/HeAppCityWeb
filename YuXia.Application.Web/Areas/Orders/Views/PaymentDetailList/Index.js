﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-09-04 14:08
 * 描  述：支付明细列表
 */
var refreshGirdData;
var bootstrap = function ($, YuXia) {
    "use strict";
    var startTime;
    var endTime;
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 时间搜索框
            $('#datesearch').lrdate({
                dfdata: [
                    { name: '今天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00') }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近7天', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'd', -6) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近1个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -1) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } },
                    { name: '近3个月', begin: function () { return YuXia.getDate('yyyy-MM-dd 00:00:00', 'm', -3) }, end: function () { return YuXia.getDate('yyyy-MM-dd 23:59:59') } }
                ],
                // 月
                mShow: false,
                premShow: false,
                // 季度
                jShow: false,
                prejShow: false,
                // 年
                ysShow: false,
                yxShow: false,
                preyShow: false,
                yShow: false,
                // 默认
                dfvalue: '1',
                selectfn: function (begin, end) {
                    startTime = begin;
                    endTime = end;
                    page.search();
                }
            });
            $('#multiple_condition_query').lrMultipleQuery(function (queryJson) {
                page.search(queryJson);
            }, 220, 400);
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                YuXia.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/Orders/PaymentDetailList/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('PaymentDetailsId');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/Orders/PaymentDetailList/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('PaymentDetailsId');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            YuXia.deleteForm(top.$.rootUrl + '/Orders/PaymentDetailList/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/Orders/PaymentDetailList/GetPageList',
                headData: [
                    { label: "支付方式", name: "PaymentMethod", width: 100, align: "left"},
                    { label: "支付金额", name: "PaymentAmount", width: 100, align: "left"},
                    { label: "支付日期", name: "PaymentDate", width: 140, align: "left"},
                    { label: "支付状态", name: "PaymentStatusStr", width: 100, align: "left"},
                    { label: "支付说明", name: "PaymentInstructions", width: 260, align: "left"},
                    { label: "订单编号", name: "OrderNumbers", width: 260, align: "left" },
                    { label: "支付用户", name: "UserName", width: 100, align: "left"},
                ],
                mainId:'PaymentDetailsId',
                reloadSelected: true,
                isPage: true
            });
        },
        search: function (param) {
            param = param || {};
            param.StartTime = startTime;
            param.EndTime = endTime;
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
