﻿using YuXia.Util;
using YuXia.Application.TwoDevelopment.Orders;
using System.Web.Mvc;
using System.Collections.Generic;

namespace YuXia.Application.Web.Areas.Orders.Controllers
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-04 14:08
    /// 描 述：支付明细列表
    /// </summary>
    public class PaymentDetailListController : MvcControllerBase
    {
        private PaymentDetailListIBLL paymentDetailListIBLL = new PaymentDetailListBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = paymentDetailListIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(int keyValue)
        {
            var PaymentDetailsData = paymentDetailListIBLL.GetPaymentDetailsEntity(keyValue);
            var UserData = paymentDetailListIBLL.GetUserEntity(PaymentDetailsData.UserId);
            var jsonData = new
            {
                PaymentDetailsData = PaymentDetailsData,
                UserData = UserData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取列表数据---支付状态
        /// </summary>
        /// <param name="parentId">父级主键</param>
        /// <param name="keyword">关键字查询（名称/编号）</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPaymentStatusEnumlist(string parentId, string keyword)
        {
            var data = paymentDetailListIBLL.EnumToList<PaymentStatusEnum>();
            return Success(data);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(int keyValue)
        {
            paymentDetailListIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string struserEntity)
        {
            PaymentDetailsEntity entity = strEntity.ToObject<PaymentDetailsEntity>();
            HeAppUserEntity userEntity = struserEntity.ToObject<HeAppUserEntity>();
            paymentDetailListIBLL.SaveEntity(keyValue, entity);
            return Success("保存成功！");
        }
        #endregion

    }
}
