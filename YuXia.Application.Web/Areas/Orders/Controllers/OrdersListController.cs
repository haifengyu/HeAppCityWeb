﻿using YuXia.Util;
using YuXia.Application.TwoDevelopment.Orders;
using System.Web.Mvc;
using System.Collections.Generic;

namespace YuXia.Application.Web.Areas.Orders.Controllers
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public class OrdersListController : MvcControllerBase
    {
        private OrdersListIBLL ordersListIBLL = new OrdersListBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = ordersListIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var OrdersData = ordersListIBLL.GetOrdersEntity( int.Parse(keyValue) );
            var OrderProductsData = ordersListIBLL.GetOrderProductsEntity( OrdersData.OrderID );
            var ProductData = ordersListIBLL.GetProductEntity( OrderProductsData.ProductID );
            var ShopData = ordersListIBLL.GetShopEntity( OrdersData.ShopID );
            var UserData = ordersListIBLL.GetUserEntity( OrdersData.UserID );
            var jsonData = new {
                OrdersData = OrdersData,
                OrderProductsData = OrderProductsData,
                ProductData = ProductData,
                ShopData = ShopData,
                UserData = UserData,
            };
            return Success(jsonData);
        }

        /// <summary>
        /// 获取列表数据---订单状态
        /// </summary>
        /// <param name="parentId">父级主键</param>
        /// <param name="keyword">关键字查询（名称/编号）</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetOrderStatusEnumlist(string parentId, string keyword)
        {
            var data = ordersListIBLL.EnumToList<OrderStatusEnum>();
            return Success(data);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            ordersListIBLL.DeleteEntity(int.Parse(keyValue));
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string strorderProductsEntity, string strproductEntity, string strshopEntity, string struserEntity)
        {
            OrdersEntity entity = strEntity.ToObject<OrdersEntity>();
            OrderProductsEntity orderProductsEntity = strorderProductsEntity.ToObject<OrderProductsEntity>();
            ProductEntity productEntity = strproductEntity.ToObject<ProductEntity>();
            ShopEntity shopEntity = strshopEntity.ToObject<ShopEntity>();
            HeAppUserEntity userEntity = struserEntity.ToObject<HeAppUserEntity>();
            ordersListIBLL.SaveEntity(keyValue,entity,orderProductsEntity,productEntity,shopEntity,userEntity);
            return Success("保存成功！");
        }
        #endregion

    }
}
