﻿using YuXia.Util;
using YuXia.Application.TwoDevelopment.Orders;
using System.Web.Mvc;
using System.Collections.Generic;

namespace YuXia.Application.Web.Areas.Orders.Controllers
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-30 14:27
    /// 描 述：商品列表
    /// </summary>
    public class ProductsListController : MvcControllerBase
    {
        private ProductsListIBLL productsListIBLL = new ProductsListBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }

        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FormEdit()
        {
            return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = productsListIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(int keyValue)
        {
            var ProductData = productsListIBLL.GetProductEntity( keyValue );
            var ProductDetailData = productsListIBLL.GetProductDetailEntity( ProductData.ProductID.Value );
            var ShopData = productsListIBLL.GetShopEntity( ProductData.ShopID );
            var jsonData = new {
                ProductData = ProductData,
                ProductDetailData = ProductDetailData,
                ShopData = ShopData,
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(int keyValue)
        {
            productsListIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string strproductDetailEntity, string strshopEntity)
        {
            ProductEntity entity = strEntity.ToObject<ProductEntity>();
            dynamic response = Newtonsoft.Json.JsonConvert.DeserializeObject(strproductDetailEntity);
            string strImageURL = response.ImageURL;
            ProductDetailEntity productDetailEntity = new ProductDetailEntity();
            productDetailEntity.ImgURL = strImageURL;
            ShopEntity shopEntity = strshopEntity.ToObject<ShopEntity>();
            productsListIBLL.SaveEntity(keyValue,entity,productDetailEntity,shopEntity);
            return Success("保存成功！");
        }
        #endregion

        #region 获取店铺数据
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetShopsList()
        {
            var ShopData = productsListIBLL.GetShopList();
            return Success(ShopData);
        }

        #endregion
    }
}
