﻿using YuXia.Util;
using YuXia.Application.TwoDevelopment.LR_CodeDemo;
using System.Web.Mvc;
using System.Collections.Generic;

namespace YuXia.Application.Web.Areas.LR_CodeDemo.Controllers
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-20 15:07
    /// 描 述：客户类测试
    /// </summary>
    public class CustomerTestController : MvcControllerBase
    {
        private CustomerTestIBLL customerTestIBLL = new CustomerTestBLL();

        #region 视图功能

        /// <summary>
        /// 主页面
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
        /// <summary>
        /// 表单页
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
             return View();
        }
        #endregion

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetPageList(string pagination, string queryJson)
        {
            Pagination paginationobj = pagination.ToObject<Pagination>();
            var data = customerTestIBLL.GetPageList(paginationobj, queryJson);
            var jsonData = new
            {
                rows = data,
                total = paginationobj.total,
                page = paginationobj.page,
                records = paginationobj.records
            };
            return Success(jsonData);
        }
        /// <summary>
        /// 获取表单数据
        /// <summary>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult GetFormData(string keyValue)
        {
            var LR_CRM_CustomerData = customerTestIBLL.GetLR_CRM_CustomerEntity( keyValue );
            var LR_CRM_CustomerContactData = customerTestIBLL.GetLR_CRM_CustomerContactEntity( LR_CRM_CustomerData.F_CustomerId );
            var jsonData = new {
                LR_CRM_CustomerData = LR_CRM_CustomerData,
                LR_CRM_CustomerContactData = LR_CRM_CustomerContactData,
            };
            return Success(jsonData);
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult DeleteForm(string keyValue)
        {
            customerTestIBLL.DeleteEntity(keyValue);
            return Success("删除成功！");
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, string strEntity, string strlR_CRM_CustomerContactEntity)
        {
            LR_CRM_CustomerEntity entity = strEntity.ToObject<LR_CRM_CustomerEntity>();
            LR_CRM_CustomerContactEntity lR_CRM_CustomerContactEntity = strlR_CRM_CustomerContactEntity.ToObject<LR_CRM_CustomerContactEntity>();
            customerTestIBLL.SaveEntity(keyValue,entity,lR_CRM_CustomerContactEntity);
            return Success("保存成功！");
        }
        #endregion

    }
}
