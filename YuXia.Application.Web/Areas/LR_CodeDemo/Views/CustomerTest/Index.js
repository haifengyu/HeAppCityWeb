﻿/* * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：超级管理员
 * 日  期：2018-08-20 15:07
 * 描  述：客户类测试
 */
var refreshGirdData;
var bootstrap = function ($, YuXia) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                YuXia.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/LR_CodeDemo/CustomerTest/Form',
                    width: 600,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('F_CustomerId');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/LR_CodeDemo/CustomerTest/Form?keyValue=' + keyValue,
                        width: 600,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#girdtable').jfGridValue('F_CustomerId');
                if (YuXia.checkrow(keyValue)) {
                    YuXia.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            YuXia.deleteForm(top.$.rootUrl + '/LR_CodeDemo/CustomerTest/DeleteForm', { keyValue: keyValue}, function () {
                                refreshGirdData();
                            });
                        }
                    });
                }
            });
        },
        // 初始化列表
        initGird: function () {
            $('#girdtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/LR_CodeDemo/CustomerTest/GetPageList',
                headData: [
                    { label: "客户主键", name: "F_CustomerId", width: 160, align: "left"},
                    { label: "客户编号", name: "F_EnCode", width: 160, align: "left"},
                    { label: "客户名称", name: "F_FullName", width: 160, align: "left"},
                    { label: "客户简称", name: "F_ShortName", width: 160, align: "left"},
                    { label: "客户行业", name: "F_CustIndustryId", width: 160, align: "left"},
                    { label: "客户类型", name: "F_CustTypeId", width: 160, align: "left"},
                    { label: "客户级别", name: "F_CustLevelId", width: 160, align: "left"},
                    { label: "所在省份", name: "F_Province", width: 160, align: "left"},
                    { label: "所在城市", name: "F_City", width: 160, align: "left"},
                    { label: "联系人", name: "F_Contact", width: 160, align: "left"},
                    { label: "手机", name: "F_Mobile", width: 160, align: "left"},
                    { label: "电话", name: "F_Tel", width: 160, align: "left"},
                ],
                mainId:'F_CustomerId',
                reloadSelected: true,
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#girdtable').jfGridSet('reload', { param: { queryJson: JSON.stringify(param) } });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
