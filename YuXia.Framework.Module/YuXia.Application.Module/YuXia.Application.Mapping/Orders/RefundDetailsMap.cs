﻿using YuXia.Application.TwoDevelopment.Orders;
using System.Data.Entity.ModelConfiguration;

namespace  YuXia.Application.Mapping
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-10 12:07
    /// 描 述：退货明细列表
    /// </summary>
    public class RefundDetailsMap : EntityTypeConfiguration<RefundDetailsEntity>
    {
        public RefundDetailsMap()
        {
            #region 表、主键
            //表
            this.ToTable("REFUNDDETAILS");
            //主键
            this.HasKey(t => t.RefundId);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

