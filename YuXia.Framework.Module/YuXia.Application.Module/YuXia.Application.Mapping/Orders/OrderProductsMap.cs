﻿using YuXia.Application.TwoDevelopment.Orders;
using System.Data.Entity.ModelConfiguration;

namespace  YuXia.Application.Mapping
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public class OrderProductsMap : EntityTypeConfiguration<OrderProductsEntity>
    {
        public OrderProductsMap()
        {
            #region 表、主键
            //表
            this.ToTable("ORDERPRODUCTS");
            //主键
            this.HasKey(t => t.OrderProductID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}

