﻿
namespace YuXia.Application.WorkFlow
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2017.04.17
    /// 描 述：工作流流转类型
    /// </summary>
    public enum WfTransportType
    {
        /// <summary>
        /// 同意
        /// </summary>
        Agree = 1,
        /// <summary>
        /// 不同意
        /// </summary>
        Disagree,
        /// <summary>
        /// 超时
        /// </summary>
        Overtime
    }
}
