﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-20 15:07
    /// 描 述：客户类测试
    /// </summary>
    public class LR_CRM_CustomerEntity 
    {
        #region 实体成员
        /// <summary>
        /// 客户主键
        /// </summary>
        [Column("F_CUSTOMERID")]
        public string F_CustomerId { get; set; }
        /// <summary>
        /// 客户编号
        /// </summary>
        [Column("F_ENCODE")]
        public string F_EnCode { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        [Column("F_FULLNAME")]
        public string F_FullName { get; set; }
        /// <summary>
        /// 客户简称
        /// </summary>
        [Column("F_SHORTNAME")]
        public string F_ShortName { get; set; }
        /// <summary>
        /// 客户行业
        /// </summary>
        [Column("F_CUSTINDUSTRYID")]
        public string F_CustIndustryId { get; set; }
        /// <summary>
        /// 客户类型
        /// </summary>
        [Column("F_CUSTTYPEID")]
        public string F_CustTypeId { get; set; }
        /// <summary>
        /// 客户级别
        /// </summary>
        [Column("F_CUSTLEVELID")]
        public string F_CustLevelId { get; set; }
        /// <summary>
        /// 客户程度
        /// </summary>
        [Column("F_CUSTDEGREEID")]
        public string F_CustDegreeId { get; set; }
        /// <summary>
        /// 所在省份
        /// </summary>
        [Column("F_PROVINCE")]
        public string F_Province { get; set; }
        /// <summary>
        /// 所在城市
        /// </summary>
        [Column("F_CITY")]
        public string F_City { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [Column("F_CONTACT")]
        public string F_Contact { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        [Column("F_MOBILE")]
        public string F_Mobile { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Column("F_TEL")]
        public string F_Tel { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        [Column("F_FAX")]
        public string F_Fax { get; set; }
        /// <summary>
        /// QQ
        /// </summary>
        [Column("F_QQ")]
        public string F_QQ { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        [Column("F_EMAIL")]
        public string F_Email { get; set; }
        /// <summary>
        /// 微信
        /// </summary>
        [Column("F_WECHAT")]
        public string F_Wechat { get; set; }
        /// <summary>
        /// 爱好
        /// </summary>
        [Column("F_HOBBY")]
        public string F_Hobby { get; set; }
        /// <summary>
        /// 法人代表
        /// </summary>
        [Column("F_LEGALPERSON")]
        public string F_LegalPerson { get; set; }
        /// <summary>
        /// 公司地址
        /// </summary>
        [Column("F_COMPANYADDRESS")]
        public string F_CompanyAddress { get; set; }
        /// <summary>
        /// 公司网站
        /// </summary>
        [Column("F_COMPANYSITE")]
        public string F_CompanySite { get; set; }
        /// <summary>
        /// 公司情况
        /// </summary>
        [Column("F_COMPANYDESC")]
        public string F_CompanyDesc { get; set; }
        /// <summary>
        /// 跟进人员Id
        /// </summary>
        [Column("F_TRACEUSERID")]
        public string F_TraceUserId { get; set; }
        /// <summary>
        /// 跟进人员
        /// </summary>
        [Column("F_TRACEUSERNAME")]
        public string F_TraceUserName { get; set; }
        /// <summary>
        /// 提醒日期
        /// </summary>
        [Column("F_ALERTDATETIME")]
        public DateTime? F_AlertDateTime { get; set; }
        /// <summary>
        /// 提醒状态
        /// </summary>
        [Column("F_ALERTSTATE")]
        public int? F_AlertState { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        [Column("F_DELETEMARK")]
        public int? F_DeleteMark { get; set; }
        /// <summary>
        /// 有效标志
        /// </summary>
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Column("F_CREATEDATE")]
        public DateTime? F_CreateDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Column("F_CREATEUSERID")]
        public string F_CreateUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Column("F_CREATEUSERNAME")]
        public string F_CreateUserName { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [Column("F_MODIFYDATE")]
        public DateTime? F_ModifyDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        [Column("F_MODIFYUSERID")]
        public string F_ModifyUserId { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Column("F_MODIFYUSERNAME")]
        public string F_ModifyUserName { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_CustomerId = Guid.NewGuid().ToString();
            this.F_CreateDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.F_CreateUserId = userInfo.userId;
            this.F_CreateUserName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_CustomerId = keyValue;
            this.F_ModifyDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.F_ModifyUserId = userInfo.userId;
            this.F_ModifyUserName = userInfo.realName;
        }
        #endregion
        #region 扩展字段
        #endregion
    }
}

