﻿using YuXia.Util;
using System.Collections.Generic;

namespace YuXia.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-20 15:07
    /// 描 述：客户类测试
    /// </summary>
    public interface CustomerTestIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<LR_CRM_CustomerEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取LR_CRM_Customer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        LR_CRM_CustomerEntity GetLR_CRM_CustomerEntity(string keyValue);
        /// <summary>
        /// 获取LR_CRM_CustomerContact表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        LR_CRM_CustomerContactEntity GetLR_CRM_CustomerContactEntity(string keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, LR_CRM_CustomerEntity entity,LR_CRM_CustomerContactEntity lR_CRM_CustomerContactEntity);
        #endregion

    }
}
