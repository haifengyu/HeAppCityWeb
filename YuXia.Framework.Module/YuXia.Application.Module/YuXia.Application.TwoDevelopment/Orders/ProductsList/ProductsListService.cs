﻿using Dapper;
using YuXia.DataBase.Repository;
using YuXia.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using YuXia.Application.Base.SystemModule;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-30 14:27
    /// 描 述：商品列表
    /// </summary>
    public class ProductsListService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<ProductEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ProductID,
                t.ProductName,
                t.Price,
                t.Count,
                t.Discreption,
                t1.ImgURL,
                t.UpdateTime,
                t2.ShopName
                ");
                strSql.Append("  FROM Product t ");
                strSql.Append("  LEFT JOIN ProductDetail t1 ON t1.ProductID = t.ProductID ");
                strSql.Append("  LEFT JOIN Shop t2 ON t2.ShopID = t.ShopID ");
                strSql.Append("  WHERE 1=1 and t1.Seq=1");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.UpdateTime >= @startTime AND t.UpdateTime <= @endTime ) ");
                }
                if (!queryParam["ProductName"].IsEmpty())
                {
                    dp.Add("ProductName", "%" + queryParam["ProductName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ProductName Like @ProductName ");
                }
                if (!queryParam["ShopName"].IsEmpty())
                {
                    dp.Add("ShopName", "%" + queryParam["ShopName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t2.ShopName Like @ShopName ");
                }
                return this.BaseRepository("HeAppCityDB").FindList<ProductEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Product表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ProductEntity GetProductEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<ProductEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取ProductDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ProductDetailEntity GetProductDetailEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<ProductDetailEntity>(t=>t.ProductID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Shop表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ShopEntity GetShopEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<ShopEntity>(t=>t.ShopID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public List<ShopEntity> GetShopList()
        {
            try
            {
                String sql = "select ShopID, ShopName from shop ;";
                return this.BaseRepository("HeAppCityDB").FindList<ShopEntity>(sql, new { }).ToList<ShopEntity>();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                var productEntity = GetProductEntity(keyValue); 
                db.Delete<ProductEntity>(t=>t.ProductID == keyValue);
                db.Delete<ProductDetailEntity>(t=>t.ProductID == productEntity.ProductID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 获取实体列表
        /// </summary>
        /// <param name="keyValues">主键值串</param>
        /// <returns></returns>
        public IEnumerable<AnnexesFileEntity> GetList(string folderId)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("SELECT * FROM LR_Base_AnnexesFile t WHERE t.F_FolderId = (@folderId) Order By t.F_CreateDate ");
                return this.BaseRepository().FindList<AnnexesFileEntity>(strSql.ToString(), new { folderId = folderId });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, ProductEntity entity,ProductDetailEntity productDetailEntity,ShopEntity shopEntity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    String now = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    entity.UpdateTime = now;
                    var strSql1 = new StringBuilder();
                    strSql1.Append("Update Product set ProductName=@ProductName,Price=@Price,Count=@Count,Discreption=@Discreption,UpdateTime=@UpdateTime where ProductID=@ProductID");
                    db.ExecuteBySql(strSql1.ToString(), new { ProductName= entity.ProductName, Price=entity.Price, Count= entity.Count, Discreption= entity.Discreption, UpdateTime = entity.UpdateTime, ProductID = keyValue });
                    var strSql2 = new StringBuilder();
                    productDetailEntity.Create();
                    IEnumerable<AnnexesFileEntity> list = GetList(productDetailEntity.ImgURL);
                    foreach (var item in list)
                    {
                        productDetailEntity.ImgURL = "products/" + item.F_Id + ".png";
                        strSql2.Append("Update ProductDetail set ImgURL=@ImgURL where ProductID=@ProductID;");
                        db.ExecuteBySql(strSql2.ToString(), new { ImgURL = productDetailEntity.ImgURL, ProductID = keyValue });
                    }
                    db.Commit();
                }
                else
                {
                    entity.UpdateTime = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    entity.ShopID = shopEntity.ShopID;
                    entity.Create();
                    db.Insert(entity);
                    db.Commit();
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("select top 1 * from product where ProductName=@ProductName and Price=@Price and Discreption=@Discreption;");
                    ProductEntity newEntity = this.BaseRepository("HeAppCityDB").FindEntity<ProductEntity>(strSql.ToString(), new { ProductName = entity.ProductName, Price = entity.Price, Discreption = entity.Discreption });
                    productDetailEntity.Create();
                    IEnumerable<AnnexesFileEntity> list= GetList(productDetailEntity.ImgURL);
                    SaveProductDetailEntity(newEntity, productDetailEntity, list);
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存产品图片
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="productDetailEntity"></param>
        /// <param name="list"></param>
        public void SaveProductDetailEntity(ProductEntity entity,ProductDetailEntity productDetailEntity,IEnumerable<AnnexesFileEntity> list)
        {
             int? i=productDetailEntity.Seq = 1;
            foreach (var item in list)
            {
                productDetailEntity.ImgURL = "products/" + item.F_Id+".png";
                productDetailEntity.ProductID = entity.ProductID.Value;
                productDetailEntity.Seq = i++;
                productDetailEntity.UpdateTime = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
                this.BaseRepository("HeAppCityDB").Insert(productDetailEntity);
            }            
        }

        #endregion

    }
}
