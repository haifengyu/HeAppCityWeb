﻿using YuXia.Util;
using System.Collections.Generic;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-30 14:27
    /// 描 述：商品列表
    /// </summary>
    public interface ProductsListIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<ProductEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取Product表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ProductEntity GetProductEntity(int keyValue);
        /// <summary>
        /// 获取ProductDetail表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ProductDetailEntity GetProductDetailEntity(int keyValue);
        /// <summary>
        /// 获取Shop表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ShopEntity GetShopEntity(int keyValue);
        /// <summary>
        /// 获取Shop表实体数据
        /// <summary>
        /// <returns></returns>
        List<ShopEntity> GetShopList();
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(int keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, ProductEntity entity,ProductDetailEntity productDetailEntity,ShopEntity shopEntity);
        #endregion

    }
}
