﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-30 14:27
    /// 描 述：商品列表
    /// </summary>
    public class ProductDetailEntity
    {
        #region 实体成员
        /// <summary>
        /// ProductDetailID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("PRODUCTDETAILID")]
        public int? ProductDetailID { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [Column("PRODUCTID")]
        public int ProductID { get; set; }
        /// <summary>
        /// Seq
        /// </summary>
        [Column("SEQ")]
        public int? Seq { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public string UpdateTime { get; set; }
        /// <summary>
        /// ImgURL
        /// </summary>
        [Column("IMGURL")]
        public string ImgURL { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int? keyValue)
        {
            this.ProductDetailID = keyValue;
        }
        #endregion
    }
}

