﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-28 16:02
    /// 描 述：订单列表
    /// </summary>
    public class ShopEntity 
    {
        #region 实体成员
        /// <summary>
        /// ShopID
        /// </summary>
        [Column("SHOPID")]
        public int ShopID { get; set; }
        /// <summary>
        /// ShopName
        /// </summary>
        [Column("SHOPNAME")]
        public string ShopName { get; set; }
        /// <summary>
        /// Tel
        /// </summary>
        [Column("TEL")]
        public string Tel { get; set; }
        /// <summary>
        /// Address
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// ShopType
        /// </summary>
        [Column("SHOPTYPE")]
        public string ShopType { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// Level
        /// </summary>
        [Column("LEVEL")]
        public int? Level { get; set; }
        /// <summary>
        /// UserID
        /// </summary>
        [Column("USERID")]
        public int UserID { get; set; }
        /// <summary>
        /// ManageID
        /// </summary>
        [Column("MANAGEID")]
        public string ManageID { get; set; }
        /// <summary>
        /// LocationScore
        /// </summary>
        [Column("LOCATIONSCORE")]
        public int? LocationScore { get; set; }
        /// <summary>
        /// ServiceScore
        /// </summary>
        [Column("SERVICESCORE")]
        public int? ServiceScore { get; set; }
        /// <summary>
        /// enviromentScore
        /// </summary>
        [Column("ENVIROMENTSCORE")]
        public int? enviromentScore { get; set; }
        /// <summary>
        /// Percapita
        /// </summary>
        [Column("PERCAPITA")]
        public int? Percapita { get; set; }
        /// <summary>
        /// BisenessTime
        /// </summary>
        [Column("BISENESSTIME")]
        public string BisenessTime { get; set; }
        /// <summary>
        /// Wifi
        /// </summary>
        [Column("WIFI")]
        public string Wifi { get; set; }
        /// <summary>
        /// DishesType
        /// </summary>
        [Column("DISHESTYPE")]
        public string DishesType { get; set; }
        /// <summary>
        /// popularity
        /// </summary>
        [Column("POPULARITY")]
        public int? popularity { get; set; }
        /// <summary>
        /// Tab
        /// </summary>
        [Column("TAB")]
        public string Tab { get; set; }
        /// <summary>
        /// ImgURL
        /// </summary>
        [Column("IMGURL")]
        public string ImgURL { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify()
        {
        }
        #endregion
    }
}

