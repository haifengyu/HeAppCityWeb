﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-28 16:02
    /// 描 述：订单列表
    /// </summary>
    public class ProductEntity 
    {
        #region 实体成员
        /// <summary>
        /// ShopID
        /// </summary>
        [Column("SHOPID")]
        public int ShopID { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [Column("PRODUCTID")]
        public int? ProductID { get; set; }
        /// <summary>
        /// ProductName
        /// </summary>
        [Column("PRODUCTNAME")]
        public string ProductName { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        [Column("PRICE")]
        public decimal Price { get; set; }
        /// <summary>
        /// popularity
        /// </summary>
        [Column("POPULARITY")]
        public int? popularity { get; set; }
        /// <summary>
        /// UpdateTime
        /// </summary>
        [Column("UPDATETIME")]
        public string UpdateTime { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        [Column("COUNT")]
        public int? Count { get; set; }
        /// <summary>
        /// Recommendation
        /// </summary>
        [Column("RECOMMENDATION")]
        public string Recommendation { get; set; }
        /// <summary>
        /// Standard
        /// </summary>
        [Column("STANDARD")]
        public string Standard { get; set; }
        /// <summary>
        /// Discreption
        /// </summary>
        [Column("DISCREPTION")]
        public string Discreption { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        [Column("TYPE")]
        public int? Type { get; set; }
        /// <summary>
        /// IsWholesale
        /// </summary>
        [Column("ISWHOLESALE")]
        public int? IsWholesale { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int? keyValue)
        {
            this.ProductID = keyValue;
        }
        #endregion

        #region 扩展字段
        /// <summary>
        /// ShopName
        /// </summary>
        [NotMapped]
        public string ShopName { get; set; }
        /// <summary>
        /// ProductName
        /// </summary>
        [NotMapped]
        public string ImgURL { get; set; }
        #endregion

    }
}

