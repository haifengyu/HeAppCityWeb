﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-28 16:02
    /// 描 述：订单列表
    /// </summary>
    public class OrdersEntity
    {
        #region 实体成员
        /// <summary>
        /// OrderID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ORDERID")]
        public int OrderID { get; set; }
        /// <summary>
        /// UserID
        /// </summary>
        [Column("USERID")]
        public int UserID { get; set; }
        /// <summary>
        /// Total
        /// </summary>
        [Column("TOTAL")]
        public decimal Total { get; set; }
        /// <summary>
        /// SubmitDate
        /// </summary>
        [Column("SUBMITDATE")]
        public DateTime? SubmitDate { get; set; }
        /// <summary>
        /// AddressID
        /// </summary>
        [Column("ADDRESSID")]
        public int AddressID { get; set; }
        /// <summary>
        /// OrderStatus
        /// </summary>
        [Column("ORDERSTATUS")]
        public int? OrderStatus { get; set; }
        /// <summary>
        /// InvoiceID
        /// </summary>
        [Column("INVOICEID")]
        public int? InvoiceID { get; set; }
        /// <summary>
        /// BuyerMessage
        /// </summary>
        [Column("BUYERMESSAGE")]
        public string BuyerMessage { get; set; }
        /// <summary>
        /// ExpressFee
        /// </summary>
        [Column("EXPRESSFEE")]
        public int? ExpressFee { get; set; }
        /// <summary>
        /// ShopID
        /// </summary>
        [Column("SHOPID")]
        public int ShopID { get; set; }
        /// <summary>
        /// OrderNum
        /// </summary>
        [Column("ORDERNUM")]
        public string OrderNum { get; set; }
        /// <summary>
        /// OrderStatusStr
        /// </summary>
        [Column("ORDERSTATUSSTR")]
        public string OrderStatusStr { get; set; }

        /// <summary>
        /// ReturnReason
        /// </summary>
        [Column("RETURNREASON")]
        public string ReturnReason { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int keyValue)
        {
            this.OrderID = keyValue;
        }
        #endregion
        #region 扩展字段
        /// <summary>
        /// Price
        /// </summary>
        [NotMapped]
        public string Price { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        [NotMapped]
        public string Count { get; set; }
        /// <summary>
        /// ShopName
        /// </summary>
        [NotMapped]
        public string ShopName { get; set; }
        /// <summary>
        /// ProductName
        /// </summary>
        [NotMapped]
        public string ProductName { get; set; }
        /// <summary>
        /// UserName
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [NotMapped]
        public string ProductID { get; set; }

        //[NotMapped]
        //public string OrderStatusStr
        //{
        //    //get
        //    //{
        //    //    return ((OrderStatusEnum)Enum.ToObject(typeof(OrderStatusEnum), OrderStatus.Value)).ToString();
        //    //}
        //    get; set;
        //}
        #endregion
    }

    public enum OrderStatusEnum
    {
        待支付 = 1,
        待发货,
        待收货,
        待评价,
        已完成,
        待退货,
        已退货
    }

    public class EnumberEntity
    {
        /// <summary>  
        /// 枚举的描述  
        /// </summary>  
        public string Desction { set; get; }

        /// <summary>  
        /// 枚举名称  
        /// </summary>  
        public string Name { set; get; }

        /// <summary>  
        /// 枚举对象的值  
        /// </summary>  
        public int Value { set; get; }
    }

    public class OrderStatusEnumberEntity
    {
        /// <summary>  
        /// 枚举的描述  
        /// </summary>  
        public string Desction { set; get; }

        /// <summary>  
        /// 枚举名称  
        /// </summary>  
        public string OrderStatusName { set; get; }

        /// <summary>  
        /// 枚举对象的值  
        /// </summary>  
        public int OrderStatus { set; get; }
    }
}

