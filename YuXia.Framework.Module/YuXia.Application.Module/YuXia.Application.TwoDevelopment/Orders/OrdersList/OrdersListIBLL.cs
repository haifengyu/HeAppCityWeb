﻿using YuXia.Util;
using System.Collections.Generic;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public interface OrdersListIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<OrdersEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取Orders表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        OrdersEntity GetOrdersEntity(int keyValue);

        /// <summary>
        /// 根据订单编号，获取订单实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        OrdersEntity GetOrdersEntity(string orderNumber);
        /// <summary>
        /// 获取OrderProducts表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        OrderProductsEntity GetOrderProductsEntity(int keyValue);
        /// <summary>
        /// 获取Product表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ProductEntity GetProductEntity(int keyValue);
        /// <summary>
        /// 获取Shop表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        ShopEntity GetShopEntity(int keyValue);
        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        HeAppUserEntity GetUserEntity(int keyValue);
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(int keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, OrdersEntity entity,OrderProductsEntity orderProductsEntity,ProductEntity productEntity,ShopEntity shopEntity, HeAppUserEntity userEntity);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, OrdersEntity entity);
        /// <summary>
        /// 更新订单状态
        /// </summary>
        /// <param name="orderNumbers"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool UpdateOrderStatus(string[] orderNumbers, OrderStatusEnum status);
        #endregion

        List<OrderStatusEnumberEntity> EnumToList<T>();
    }
}
