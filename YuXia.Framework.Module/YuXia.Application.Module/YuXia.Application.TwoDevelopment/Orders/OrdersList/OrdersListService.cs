﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using YuXia.DataBase.Repository;
using YuXia.Util;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public class OrdersListService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<OrdersEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.OrderID,
                t.OrderNum,
                t.OrderStatusStr,
                --case t.OrderStatus when 1 then '待支付' when 2 then '待发货' when 3 then '待收货' when 4 then '待评价' else '其它状态' end OrderStatusStr,
                t1.Price,
                t1.Count,
                t.ExpressFee,
                t.Total,
                t2.ShopName,
                t3.ProductName,
                t4.UserName,
                t.SubmitDate,
                t.ShopID,
                t1.ProductID,
                t.UserID,
                t.OrderStatus
                ");
                strSql.Append("  FROM Orders t ");
                strSql.Append("  LEFT JOIN OrderProducts t1 ON t1.OrderID = t.OrderID ");
                strSql.Append("  LEFT JOIN Shop t2 ON t2.ShopID = t.ShopID ");
                strSql.Append("  LEFT JOIN Product t3 ON t1.ProductID = t3.ProductID ");
                strSql.Append("  LEFT JOIN [User] t4 ON t4.UserID = t.UserID ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.SubmitDate >= @startTime AND t.SubmitDate <= @endTime ) ");
                }
                if (!queryParam["OrderNum"].IsEmpty())
                {
                    dp.Add("OrderNum", "%" + queryParam["OrderNum"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderNum Like @OrderNum ");
                }
                if (!queryParam["OrderStatus"].IsEmpty())
                {
                    dp.Add("OrderStatus", "%" + queryParam["OrderStatus"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderStatus Like @OrderStatus ");
                }
                if (!queryParam["ShopName"].IsEmpty())
                {
                    dp.Add("ShopName", "%" + queryParam["ShopName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t2.ShopName Like @ShopName ");
                }
                if (!queryParam["UserName"].IsEmpty())
                {
                    dp.Add("UserName", "%" + queryParam["UserName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t4.UserName Like @UserName ");
                }
                if (!queryParam["ProductName"].IsEmpty())
                {
                    dp.Add("ProductName", "%" + queryParam["ProductName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t3.ProductName Like @ProductName ");
                }
                return this.BaseRepository("HeAppCityDB").FindList<OrdersEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Orders表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrdersEntity GetOrdersEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<OrdersEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据订单编号，获取订单实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public OrdersEntity GetOrdersEntity(string orderNumber)
        {
            try
            {
                string sql = @"select * from Orders where OrderNum = @OrderNumbers";
                var dp = new DynamicParameters(new { });
                dp.Add("OrderNumbers", orderNumber, DbType.String);
                return this.BaseRepository("HeAppCityDB").FindEntity<OrdersEntity>(sql, dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取OrderProducts表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrderProductsEntity GetOrderProductsEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<OrderProductsEntity>(t=>t.OrderID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Product表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ProductEntity GetProductEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<ProductEntity>(t=>t.ProductID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Shop表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public ShopEntity GetShopEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<ShopEntity>(t=>t.ShopID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public HeAppUserEntity GetUserEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<HeAppUserEntity>(t=>t.UserID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                var ordersEntity = GetOrdersEntity(keyValue); 
                var orderProductsEntity = GetOrderProductsEntity(ordersEntity.OrderID); 
                db.Delete<OrdersEntity>(t=>t.OrderID == keyValue);
                db.Delete<OrderProductsEntity>(t=>t.OrderID == ordersEntity.OrderID);
                db.Delete<ProductEntity>(t=>t.ProductID == orderProductsEntity.ProductID);
                db.Delete<ShopEntity>(t=>t.ShopID == ordersEntity.ShopID);
                db.Delete<HeAppUserEntity>(t=>t.UserID == ordersEntity.UserID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, OrdersEntity entity,OrderProductsEntity orderProductsEntity,ProductEntity productEntity,ShopEntity shopEntity, HeAppUserEntity userEntity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    var ordersEntityTmp = GetOrdersEntity(int.Parse(keyValue)); 
                    var orderProductsEntityTmp = GetOrderProductsEntity(ordersEntityTmp.OrderID); 
                    entity.Modify(int.Parse(keyValue));
                    db.Update(entity);
                    db.Delete<OrderProductsEntity>(t=>t.OrderID == ordersEntityTmp.OrderID);
                    orderProductsEntity.Create();
                    orderProductsEntity.OrderID = ordersEntityTmp.OrderID;
                    db.Insert(orderProductsEntity);
                    db.Delete<ProductEntity>(t=>t.ProductID == orderProductsEntityTmp.ProductID);
                    productEntity.Create();
                    productEntity.ProductID = orderProductsEntityTmp.ProductID;
                    db.Insert(productEntity);
                    db.Delete<ShopEntity>(t=>t.ShopID == ordersEntityTmp.ShopID);
                    shopEntity.Create();
                    shopEntity.ShopID = ordersEntityTmp.ShopID;
                    db.Insert(shopEntity);
                    db.Delete<HeAppUserEntity>(t=>t.UserID == ordersEntityTmp.UserID);
                    userEntity.Create();
                    userEntity.UserID = ordersEntityTmp.UserID;
                    db.Insert(userEntity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    orderProductsEntity.Create();
                    orderProductsEntity.OrderID = entity.OrderID;
                    db.Insert(orderProductsEntity);
                    productEntity.Create();
                    productEntity.ProductID = orderProductsEntity.ProductID;
                    db.Insert(productEntity);
                    shopEntity.Create();
                    shopEntity.ShopID = entity.ShopID;
                    db.Insert(shopEntity);
                    userEntity.Create();
                    userEntity.UserID = entity.UserID;
                    db.Insert(userEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, OrdersEntity entity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    //var paymentDetailsEntityTmp = GetPaymentDetailsEntity(int.Parse(keyValue)); 
                    entity.Modify(int.Parse(keyValue));
                    db.Update(entity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 更新订单状态
        /// </summary>
        /// <param name="orderNumbers"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateOrderStatus(string[] orderNumbers, OrderStatusEnum status)
        {
            bool result = false;
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                var dataList = db.FindList<OrdersEntity>(t => orderNumbers.Contains(t.OrderNum)).ToList();
                foreach (var item in dataList)
                {
                    item.OrderStatus = (int)status;
                    item.OrderStatusStr = status.ToString();
                    db.Update<OrdersEntity>(item);                   
                }

                db.Commit();
                result = true; 
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
            return result;
        }
        #endregion

    }
}
