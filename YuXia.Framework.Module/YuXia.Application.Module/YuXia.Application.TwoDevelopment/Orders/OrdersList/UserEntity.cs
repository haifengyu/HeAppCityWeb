﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public class HeAppUserEntity
    {
        #region 实体成员
        /// <summary>
        /// UserID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("USERID")]
        public int UserID { get; set; }
        /// <summary>
        /// UserName
        /// </summary>
        [Column("USERNAME")]
        public string UserName { get; set; }
        /// <summary>
        /// Experience
        /// </summary>
        [Column("EXPERIENCE")]
        public string Experience { get; set; }
        /// <summary>
        /// AddTime
        /// </summary>
        [Column("ADDTIME")]
        public DateTime? AddTime { get; set; }
        /// <summary>
        /// Logo
        /// </summary>
        [Column("LOGO")]
        public string Logo { get; set; }
        /// <summary>
        /// Level
        /// </summary>
        [Column("LEVEL")]
        public string Level { get; set; }
        /// <summary>
        /// ImgURL
        /// </summary>
        [Column("IMGURL")]
        public string ImgURL { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        [Column("PASSWORD")]
        public string Password { get; set; }
        /// <summary>
        /// IsMember
        /// </summary>
        [Column("ISMEMBER")]
        public bool? IsMember { get; set; }
        /// <summary>
        /// MembershipLevel
        /// </summary>
        [Column("MEMBERSHIPLEVEL")]
        public int? MembershipLevel { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify()
        {
        }
        #endregion
    }
}

