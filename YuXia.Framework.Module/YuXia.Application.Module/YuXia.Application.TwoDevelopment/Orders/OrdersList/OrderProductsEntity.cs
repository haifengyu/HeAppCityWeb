﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-29 17:06
    /// 描 述：订单列表查看
    /// </summary>
    public class OrderProductsEntity 
    {
        #region 实体成员
        /// <summary>
        /// OrderProductID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        [Column("ORDERPRODUCTID")]
        public int? OrderProductID { get; set; }
        /// <summary>
        /// OrderID
        /// </summary>
        [Column("ORDERID")]
        public int? OrderID { get; set; }
        /// <summary>
        /// ProductID
        /// </summary>
        [Column("PRODUCTID")]
        public int ProductID { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        [Column("COUNT")]
        public int? Count { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        [Column("PRICE")]
        public Single? Price { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int? keyValue)
        {
            this.OrderProductID = keyValue;
        }
        #endregion
    }
}

