﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-10 12:07
    /// 描 述：退货明细列表
    /// </summary>
    public class OrderNumTempEntity 
    {
        #region 实体成员
        /// <summary>
        /// OrderNum
        /// </summary>
        [Column("ORDERNUM")]
        public string OrderNum { get; set; }
        /// <summary>
        /// UserID
        /// </summary>
        [Column("USERID")]
        public int UserID { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int keyValue)
        {
            this.UserID = keyValue;
        }
        #endregion
    }
}

