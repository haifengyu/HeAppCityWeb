﻿using YuXia.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-10 12:08
    /// 描 述：退货明细列表
    /// </summary>
    public class RefundDetailListBLL : RefundDetailListIBLL
    {
        private RefundDetailListService refundDetailListService = new RefundDetailListService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<RefundDetailsEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return refundDetailListService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取RefundDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntity(int keyValue)
        {
            try
            {
                return refundDetailListService.GetRefundDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据退款订单编号，获取退款实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntity(string orderNumber)
        {
            try
            {
                return refundDetailListService.GetRefundDetailsEntity(orderNumber);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据订单编号，获取退款实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntityByOrderNumber(string orderNumber)
        {
            try
            {
                return refundDetailListService.GetRefundDetailsEntityByOrderNumber(orderNumber);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Orders表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrdersEntity GetOrdersEntity(int keyValue)
        {
            try
            {
                return refundDetailListService.GetOrdersEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public HeAppUserEntity GetUserEntity(int keyValue)
        {
            try
            {
                return refundDetailListService.GetUserEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(int keyValue)
        {
            try
            {
                return refundDetailListService.GetPaymentDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取OrderNumTemp表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrderNumTempEntity GetOrderNumTempEntity(int keyValue)
        {
            try
            {
                return refundDetailListService.GetOrderNumTempEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取订单状态枚举类型列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<RefundStatusEnumberEntity> EnumToList<T>()
        {
            List<RefundStatusEnumberEntity> list = new List<RefundStatusEnumberEntity>();

            foreach (var e in Enum.GetValues(typeof(T)))
            {
                RefundStatusEnumberEntity m = new RefundStatusEnumberEntity();
                object[] objArr = e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (objArr != null && objArr.Length > 0)
                {
                    DescriptionAttribute da = objArr[0] as DescriptionAttribute;
                    m.Desction = da.Description;
                }
                m.RefundStatus = Convert.ToInt32(e);
                m.RefundStatusName = e.ToString();
                list.Add(m);
            }
            return list;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, RefundDetailsEntity entity,OrdersEntity ordersEntity, HeAppUserEntity userEntity,PaymentDetailsEntity paymentDetailsEntity)
        {
            try
            {
                refundDetailListService.SaveEntity(keyValue, entity,ordersEntity,userEntity,paymentDetailsEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveRefundDetailsEntity(string keyValue, RefundDetailsEntity entity)
        {
            try
            {
                refundDetailListService.SaveRefundDetailsEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
