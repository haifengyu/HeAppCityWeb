﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-10 12:07
    /// 描 述：退货明细列表
    /// </summary>
    public class RefundDetailsEntity 
    {
        #region 实体成员
        /// <summary>
        /// RefundId
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        [Column("REFUNDID")]
        public int RefundId { get; set; }
        /// <summary>
        /// RefundNumber
        /// </summary>
        [Column("REFUNDNUMBER")]
        public string RefundNumber { get; set; }
        /// <summary>
        /// RefundMethod
        /// </summary>
        [Column("REFUNDMETHOD")]
        public string RefundMethod { get; set; }
        /// <summary>
        /// RefundAmount
        /// </summary>
        [Column("REFUNDAMOUNT")]
        public decimal RefundAmount { get; set; }
        /// <summary>
        /// RefundDate
        /// </summary>
        [Column("REFUNDDATE")]
        public DateTime RefundDate { get; set; }
        /// <summary>
        /// RefundStatus
        /// </summary>
        [Column("REFUNDSTATUS")]
        public Int16 RefundStatus { get; set; }
        /// <summary>
        /// OrderId
        /// </summary>
        [Column("ORDERID")]
        public int OrderId { get; set; }
        /// <summary>
        /// OrderNumbers
        /// </summary>
        [Column("ORDERNUMBERS")]
        public string OrderNumbers { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        [Column("USERID")]
        public int UserId { get; set; }
        /// <summary>
        /// TradeNo
        /// </summary>
        [Column("TRADENO")]
        public string TradeNo { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int keyValue)
        {
            this.RefundId = keyValue;
        }
        #endregion
        #region 扩展字段
        /// <summary>
        /// ReturnReason
        /// </summary>
        [NotMapped]
        public string ReturnReason { get; set; }
        /// <summary>
        /// OrderStatus
        /// </summary>
        [NotMapped]
        public int OrderStatus { get; set; }
        /// <summary>
        /// UserName
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }
        /// <summary>
        /// PaymentStatus
        /// </summary>
        [NotMapped]
        public int PaymentStatus { get; set; }
        /// <summary>
        /// PaymentStatusStr
        /// </summary>
        [NotMapped]
        public string PaymentStatusStr
        {
            get
            {
                return ((PaymentStatusEnum)PaymentStatus).ToString();
            }
        }
        /// <summary>
        /// OrderStatusStr
        /// </summary>
        [NotMapped]
        public string OrderStatusStr
        {
            get
            {
                return ((OrderStatusEnum)OrderStatus).ToString();
            }
        }
        /// <summary>
        /// RefundStatusStr
        /// </summary>
        [Column("REFUNDSTATUS")]
        public string RefundStatusStr
        {

            get
            {
                return ((RefundStatusEnum)RefundStatus).ToString();
            }
        }
        #endregion
    }

    public enum RefundStatusEnum
    {
        待退款 = 1,
        已退款,
        退款失败
    }


    public class RefundStatusEnumberEntity
    {
        /// <summary>  
        /// 枚举的描述  
        /// </summary>  
        public string Desction { set; get; }

        /// <summary>  
        /// 枚举名称  
        /// </summary>  
        public string RefundStatusName { set; get; }

        /// <summary>  
        /// 枚举对象的值  
        /// </summary>  
        public int RefundStatus { set; get; }
    }
}

