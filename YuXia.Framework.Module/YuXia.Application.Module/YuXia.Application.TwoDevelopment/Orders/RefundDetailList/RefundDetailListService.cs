﻿using Dapper;
using YuXia.DataBase.Repository;
using YuXia.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-10 12:08
    /// 描 述：退货明细列表
    /// </summary>
    public class RefundDetailListService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<RefundDetailsEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.RefundId,
                t.RefundNumber,
                t.RefundMethod,
                t.RefundAmount,
                t.RefundDate,
                t.RefundStatus,
                t.OrderId,
                t.OrderNumbers,
                t.UserId,
                t.TradeNo,
                t1.ReturnReason,
                t1.OrderStatus,
                t2.UserName,
                t3.PaymentStatus
                ");
                strSql.Append("  FROM RefundDetails t ");
                strSql.Append("  LEFT JOIN Orders t1 ON t1.OrderID = t.OrderId ");
                strSql.Append("  LEFT JOIN [User] t2 ON t2.UserID = t.UserId ");
                strSql.Append("  LEFT JOIN PaymentDetails t3 ON t3.PaymentStatus=2 and CHARINDEX(','+t3.OrderNumbers+',', ','+t.OrderNumbers+',')>0 ");
                strSql.Append("  WHERE 1=1 and t.RefundStatus!=3 and t.RefundNumber not in (select RefundNumber from RefundDetails temp where RefundStatus =2 and  temp.OrderNumbers = t.OrderNumbers) ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["RefundStatus"].IsEmpty())
                {
                    dp.Add("RefundStatus", "%" + queryParam["RefundStatus"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.RefundStatus Like @RefundStatus ");
                }
                if (!queryParam["OrderNumbers"].IsEmpty())
                {
                    dp.Add("OrderNumbers", "%" + queryParam["OrderNumbers"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.OrderNumbers Like @OrderNumbers ");
                }
                if (!queryParam["OrderStatus"].IsEmpty())
                {
                    dp.Add("OrderStatus", "%" + queryParam["OrderStatus"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t1.OrderStatus Like @OrderStatus ");
                }
                if (!queryParam["PaymentStatus"].IsEmpty())
                {
                    dp.Add("PaymentStatus", "%" + queryParam["PaymentStatus"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t3.PaymentStatus Like @PaymentStatus ");
                }
                if (!queryParam["UserName"].IsEmpty())
                {
                    dp.Add("UserName", "%" + queryParam["UserName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t2.UserName Like @UserName ");
                }
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.RefundDate >= @startTime AND t.RefundDate <= @endTime ) ");
                }
                return this.BaseRepository("HeAppCityDB").FindList<RefundDetailsEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取RefundDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<RefundDetailsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据退款订单编号，获取退款实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntity(string orderNumber)
        {
            try
            {
                string sql = @"select * from RefundDetails where RefundNumber = @OrderNumbers";
                var dp = new DynamicParameters(new { });
                dp.Add("OrderNumbers", orderNumber, DbType.String);
                return this.BaseRepository("HeAppCityDB").FindEntity<RefundDetailsEntity>(sql, dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据订单编号，获取退款实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public RefundDetailsEntity GetRefundDetailsEntityByOrderNumber(string orderNumber)
        {
            try
            {
                string sql = @"select * from RefundDetails where RefundStatus =1 and OrderNumbers = @OrderNumbers";
                var dp = new DynamicParameters(new { });
                dp.Add("OrderNumbers", orderNumber, DbType.String);
                return this.BaseRepository("HeAppCityDB").FindEntity<RefundDetailsEntity>(sql, dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Orders表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrdersEntity GetOrdersEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<OrdersEntity>(t=>t.OrderID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public HeAppUserEntity GetUserEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<HeAppUserEntity>(t=>t.UserID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<PaymentDetailsEntity>(t=>t.PaymentDetailsId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取OrderNumTemp表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public OrderNumTempEntity GetOrderNumTempEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<OrderNumTempEntity>(t=>t.UserID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据
        
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, RefundDetailsEntity entity,OrdersEntity ordersEntity, HeAppUserEntity userEntity,PaymentDetailsEntity paymentDetailsEntity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    var refundDetailsEntityTmp = GetRefundDetailsEntity(int.Parse(keyValue)); 
                    var userEntityTmp = GetUserEntity(refundDetailsEntityTmp.UserId); 
                    entity.Modify(int.Parse(keyValue));
                    db.Update(entity);
                    db.Delete<OrdersEntity>(t=>t.OrderID == refundDetailsEntityTmp.OrderId);
                    ordersEntity.Create();
                    ordersEntity.OrderID = refundDetailsEntityTmp.OrderId;
                    db.Insert(ordersEntity);
                    db.Delete<HeAppUserEntity>(t=>t.UserID == refundDetailsEntityTmp.UserId);
                    userEntity.Create();
                    userEntity.UserID = refundDetailsEntityTmp.UserId;
                    db.Insert(userEntity);
                    db.Delete<PaymentDetailsEntity>(t=>t.OrderNumbers == refundDetailsEntityTmp.OrderNumbers);
                    paymentDetailsEntity.Create();
                    paymentDetailsEntity.OrderNumbers = refundDetailsEntityTmp.OrderNumbers;
                    db.Insert(paymentDetailsEntity);
                    db.Delete<OrderNumTempEntity>(t=>t.UserID == userEntityTmp.UserID);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    ordersEntity.Create();
                    ordersEntity.OrderID = entity.OrderId;
                    db.Insert(ordersEntity);
                    userEntity.Create();
                    userEntity.UserID = entity.UserId;
                    db.Insert(userEntity);
                    paymentDetailsEntity.Create();
                    paymentDetailsEntity.OrderNumbers = entity.OrderNumbers;
                    db.Insert(paymentDetailsEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）--RefundDetails
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveRefundDetailsEntity(string keyValue, RefundDetailsEntity entity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(int.Parse(keyValue));
                    db.Update(entity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
