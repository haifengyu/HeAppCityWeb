﻿using YuXia.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-04 14:08
    /// 描 述：支付明细列表
    /// </summary>
    public class PaymentDetailsEntity
    {
        #region 实体成员
        /// <summary>
        /// PaymentDetailsId
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("PAYMENTDETAILSID")]
        public int PaymentDetailsId { get; set; }
        /// <summary>
        /// PaymentMethod
        /// </summary>
        [Column("PAYMENTMETHOD")]
        public string PaymentMethod { get; set; }
        /// <summary>
        /// PaymentAmount
        /// </summary>
        [Column("PAYMENTAMOUNT")]
        public decimal PaymentAmount { get; set; }
        /// <summary>
        /// PaymentDate
        /// </summary>
        [Column("PAYMENTDATE")]
        public DateTime PaymentDate { get; set; }
        /// <summary>
        /// PaymentStatus
        /// </summary>
        [Column("PAYMENTSTATUS")]
        public int PaymentStatus { get; set; }
        /// <summary>
        /// PaymentInstructions
        /// </summary>
        [Column("PAYMENTINSTRUCTIONS")]
        public string PaymentInstructions { get; set; }
        /// <summary>
        /// OrderId
        /// </summary>
        [Column("ORDERID")]
        public int? OrderId { get; set; }
        /// <summary>
        /// OrderNumbers
        /// </summary>
        [Column("ORDERNUMBERS")]
        public string OrderNumbers { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        [Column("USERID")]
        public int UserId { get; set; }

        /// <summary>
        /// 第三方交易号
        /// </summary>
        [Column("TRADENO")]
        public string TradeNo { get; set; }

        /// <summary>
        /// ReceiptAmount  -- 实收金额
        /// </summary>
        [Column("RECEIPTAMOUNT")]
        public decimal ReceiptAmount { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(int keyValue)
        {
            this.PaymentDetailsId = keyValue;
        }
        #endregion
        #region 扩展字段
        /// <summary>
        /// UserName
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }
        /// <summary>
        /// ImgURL
        /// </summary>
        [NotMapped]
        public string ImgURL { get; set; }

        /// <summary>
        /// ImgURL
        /// </summary>
        [NotMapped]
        public string PaymentStatusStr
        {
            get
            {
                return ((PaymentStatusEnum)PaymentStatus).ToString();
            }
        }
        #endregion
    }

    /// <summary>
    /// 订单状态
    /// </summary>
    public enum PaymentStatusEnum
    {
        未支付 = 1,
        已支付,        
        支付失败,
    }



    public class PaymentStatusEnumberEntity
    {
        /// <summary>  
        /// 枚举的描述  
        /// </summary>  
        public string Desction { set; get; }

        /// <summary>  
        /// 枚举名称  
        /// </summary>  
        public string PaymentStatusName { set; get; }

        /// <summary>  
        /// 枚举对象的值  
        /// </summary>  
        public int PaymentStatus { set; get; }
    }
}

