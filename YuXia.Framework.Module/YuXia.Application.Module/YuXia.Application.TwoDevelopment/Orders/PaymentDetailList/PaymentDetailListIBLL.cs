﻿using YuXia.Util;
using System.Collections.Generic;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-04 14:08
    /// 描 述：支付明细列表
    /// </summary>
    public interface PaymentDetailListIBLL
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<PaymentDetailsEntity> GetPageList(Pagination pagination, string queryJson);
        /// <summary>
        /// 获取PaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PaymentDetailsEntity GetPaymentDetailsEntity(int keyValue);

        /// <summary>
        /// 根据订单编号，获取支付实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        PaymentDetailsEntity GetPaymentDetailsEntity(string orderNumber);
        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        HeAppUserEntity GetUserEntity(int keyValue);

        /// <summary>
        /// 获取订单状态枚举类型列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        List<PaymentStatusEnumberEntity> EnumToList<T>();
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(int keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, PaymentDetailsEntity entity);
        #endregion

    }
}
