﻿using Dapper;
using YuXia.DataBase.Repository;
using YuXia.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-04 14:08
    /// 描 述：支付明细列表
    /// </summary>
    public class PaymentDetailListService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<PaymentDetailsEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var sqlCondition = new StringBuilder();
                var sql = @"SELECT t.PaymentDetailsId,
                                    t.PaymentMethod,
                                    t.PaymentAmount,
                                    t.PaymentDate,
                                    t.PaymentStatus,
                                    t.PaymentInstructions,
                                    t.OrderId,
                                    t.OrderNumbers,
                                    t.UserId,
                                    t1.UserName,
                                    t1.ImgURL
				                from
                                (
                                select *, ROW_NUMBER() over(partition by OrderNumbers order by PaymentStatus desc) as rowNum
                                from PaymentDetails
                                ) t
                                LEFT JOIN [User] t1 ON t1.UserID = t.UserId
                                where t.rowNum <= 1 {0} ";
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    sqlCondition.Append(" AND ( t.PaymentDate >= @startTime AND t.PaymentDate <= @endTime ) ");
                }
                if (!queryParam["UserName"].IsEmpty())
                {
                    dp.Add("UserName", "%" + queryParam["UserName"].ToString() + "%", DbType.String);
                    sqlCondition.Append(" AND t1.UserName Like @UserName ");
                }

                string strSql = string.Format(sql, sqlCondition.ToString());

                return this.BaseRepository("HeAppCityDB").FindList<PaymentDetailsEntity>(strSql, dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<PaymentDetailsEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根据订单编号，获取支付实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(string orderNumber)
        {
            try
            {
                string sql = @"select * from PaymentDetails where OrderNumbers = @OrderNumbers";
                var dp = new DynamicParameters(new { });
                dp.Add("OrderNumbers", orderNumber, DbType.String);
                return this.BaseRepository("HeAppCityDB").FindEntity<PaymentDetailsEntity>(sql, dp);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public HeAppUserEntity GetUserEntity(int keyValue)
        {
            try
            {
                return this.BaseRepository("HeAppCityDB").FindEntity<HeAppUserEntity>(t=>t.UserID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                var paymentDetailsEntity = GetPaymentDetailsEntity(keyValue); 
                db.Delete<PaymentDetailsEntity>(t=>t.PaymentDetailsId == keyValue);
                db.Delete<HeAppUserEntity>(t=>t.UserID == paymentDetailsEntity.UserId);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, PaymentDetailsEntity entity)
        {
            var db = this.BaseRepository("HeAppCityDB").BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    //var paymentDetailsEntityTmp = GetPaymentDetailsEntity(int.Parse(keyValue)); 
                    entity.Modify(int.Parse(keyValue));
                    db.Update(entity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
