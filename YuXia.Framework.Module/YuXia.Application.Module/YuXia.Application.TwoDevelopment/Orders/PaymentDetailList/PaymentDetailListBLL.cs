﻿using YuXia.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace YuXia.Application.TwoDevelopment.Orders
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-09-04 14:08
    /// 描 述：支付明细列表
    /// </summary>
    public class PaymentDetailListBLL : PaymentDetailListIBLL
    {
        private PaymentDetailListService paymentDetailListService = new PaymentDetailListService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<PaymentDetailsEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return paymentDetailListService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PaymentDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(int keyValue)
        {
            try
            {
                return paymentDetailListService.GetPaymentDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根据订单编号，获取支付实体对象
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public PaymentDetailsEntity GetPaymentDetailsEntity(string orderNumber)
        {
            try
            {
                return paymentDetailListService.GetPaymentDetailsEntity(orderNumber);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取User表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public HeAppUserEntity GetUserEntity(int keyValue)
        {
            try
            {
                return paymentDetailListService.GetUserEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取订单状态枚举类型列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<PaymentStatusEnumberEntity> EnumToList<T>()
        {
            List<PaymentStatusEnumberEntity> list = new List<PaymentStatusEnumberEntity>();

            foreach (var e in Enum.GetValues(typeof(T)))
            {
                PaymentStatusEnumberEntity m = new PaymentStatusEnumberEntity();
                object[] objArr = e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (objArr != null && objArr.Length > 0)
                {
                    DescriptionAttribute da = objArr[0] as DescriptionAttribute;
                    m.Desction = da.Description;
                }
                m.PaymentStatus = Convert.ToInt32(e);
                m.PaymentStatusName = e.ToString();
                list.Add(m);
            }
            return list;
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(int keyValue)
        {
            try
            {
                paymentDetailListService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, PaymentDetailsEntity entity)
        {
            try
            {
                paymentDetailListService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
